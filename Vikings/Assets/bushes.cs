﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bushes : MonoBehaviour
{
    
    [FMODUnity.EventRef] public string bushesevent;
    private FMOD.Studio.EventInstance bushesinstance;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        bushesinstance = FMODUnity.RuntimeManager.CreateInstance(bushesevent);
        bushesinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        bushesinstance.start();
        bushesinstance.release();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
