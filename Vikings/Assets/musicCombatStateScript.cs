﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Invector.vCharacterController.AI;

public class musicCombatStateScript : MonoBehaviour
{

    public vControlAIMelee enemyController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyController.isInCombat == true)
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName("CombatState", 1f);
            //Debug.Log("True");
        }
        else
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName("CombatState", 0f);
            //Debug.Log("NOT");
        }
        
    }
}
