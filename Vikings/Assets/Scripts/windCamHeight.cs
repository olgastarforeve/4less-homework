﻿using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;
using UnityEngine.UIElements;
using Invector.vCharacterController;

public class windCamHeight : MonoBehaviour
{
    
    public GameObject camera;
    [FMODUnity.EventRef] public string windevent; //

    private FMOD.Studio.EventInstance windinstance; //сюда передается параметр позиции камеры 

    public DayNightController dnc; //контроллер для дня и ночи
   // public GameObject daynight;
    
    
    // Start is called before the first frame update
    void Start()
    {
        windinstance = FMODUnity.RuntimeManager.CreateInstance(windevent); //создали ветер и запустили его на старте
        windinstance.start(); //етер в 2D поэтому параметры 3D передaвать не нужно

        //dnc = daynight.GetComponent<DayNightController>(); //десь хранится наш контроллер через которого можно искать текущее времся суток в игре
    }

    
    // Update is called once per frame
    void Update()
    {
        windinstance.setParameterByName("cam_height", camera.transform.position.y);  //отслеживаем каждый раз где находится камера и передавать ему параметр высоты
        windinstance.setParameterByName("daynight", dnc.currentTime);  //для локального значения из мода
        //FMODUnity.RuntimeManager.StudioSystem.setParameterByName("daynight", dnc.currentTime); //для глобального значения из ефмода
        
        //Debug.Log(camera.transform.position.y);
       // Debug.Log(dnc.currentTime);
    }
    
    
}
