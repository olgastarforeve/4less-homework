﻿using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;
using UnityEngine.UIElements;
using Invector.vCharacterController;

public class Footsteps_surface : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string footstepsevent;

    public vThirdPersonInput charInput;
    
    public LayerMask layer;
    private float typeSurface;

    [FMODUnity.EventRef] public string jumpevent;
    [FMODUnity.EventRef] public string landevent;
    [FMODUnity.EventRef] public string runevent;
    public bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        charInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    void footstep()
    {

        if (charInput.cc.inputMagnitude > 0.1)
        {
            
            MaterialCheck();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("isRun", charInput.cc.inputMagnitude);

            eventInstance.setParameterByName("surface_type", typeSurface);
            eventInstance.start();
            eventInstance.release();
            
        }

    }


    void MaterialCheck()
    {
        RaycastHit rayHit;
       
        if (Physics.Raycast(transform.position, Vector3.down, out rayHit, 0.3f, layer))
        {
            //Debug.Log(rayHit.collider.tag);
            if (rayHit.collider.CompareTag("vSnow")) typeSurface = 0;
            else if (rayHit.collider.CompareTag("vWood")) typeSurface = 1;
            else if (rayHit.collider.CompareTag("vWater")) typeSurface = 2;
            else if (rayHit.collider.CompareTag("vCrunch")) typeSurface = 3;
            else if (rayHit.collider.CompareTag("vRock")) typeSurface = 4;
            else if (rayHit.collider.CompareTag("vNearWater")) typeSurface = 5;
            else if (rayHit.collider.CompareTag("vIceStead")) typeSurface = 6;
            else if (rayHit.collider.CompareTag("vIce")) typeSurface = 7;
            else if (rayHit.collider.CompareTag("vSnowWood")) typeSurface = 8;
            else typeSurface = 0;

        }
    }
    
    
    void run()
    {
        if (charInput.cc.inputMagnitude > 1.3)
        {
            MaterialCheck();
            Debug.Log("run");
            FMOD.Studio.EventInstance runeventInstance = FMODUnity.RuntimeManager.CreateInstance(runevent);
            runeventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            runeventInstance.setParameterByName("surface_type", typeSurface);
            runeventInstance.start();
            runeventInstance.release();
        }
       
        
    }

    void jump()
    {
        if (isGrounded == false)
        {
            MaterialCheck();
            //Debug.Log("Jump");
            FMOD.Studio.EventInstance jumpEventInstance = FMODUnity.RuntimeManager.CreateInstance(jumpevent);
            jumpEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            jumpEventInstance.setParameterByName("surface_type", typeSurface);
            jumpEventInstance.start();
            jumpEventInstance.release();
        }
        
    }
    
    void land()
    {
        
        
            MaterialCheck();
            //Debug.Log("Land");
            FMOD.Studio.EventInstance landeventInstance = FMODUnity.RuntimeManager.CreateInstance(landevent);
            landeventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            landeventInstance.setParameterByName("surface_type", typeSurface);
            landeventInstance.start();
            landeventInstance.release();
        
    }
    
    
    
    
   
    
    
}























