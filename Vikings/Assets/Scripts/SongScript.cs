﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongScript : MonoBehaviour
{
    
    [FMODUnity.EventRef] public string songevent;
    private FMOD.Studio.EventInstance songinstance;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        songinstance = FMODUnity.RuntimeManager.CreateInstance(songevent);
        songinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        songinstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
