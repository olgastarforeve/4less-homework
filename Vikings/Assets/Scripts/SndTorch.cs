﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SndTorch : MonoBehaviour
{

    [FMODUnity.EventRef] public string torchevent;

    private FMOD.Studio.EventInstance torchinstance;
    
    
    void Start()
    {
        torchinstance = FMODUnity.RuntimeManager.CreateInstance(torchevent);
        torchinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        torchinstance.start();
        
        // FMODUnity.RuntimeManager.PlayOneShot(torchevent, gameObject.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
