﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;



public class low_health : MonoBehaviour
{

    [FMODUnity.EventRef] public string snapevent;
    private FMOD.Studio.EventInstance snapInstance;
    public vThirdPersonController control;
    public bool SnapshotIsPlaying = false;
    public FMOD.Studio.PLAYBACK_STATE playback; //проверка стейта для текущей переменной
    public vControlAIMelee aicontrol;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        control = gameObject.GetComponent<vThirdPersonController>();
        snapInstance = FMODUnity.RuntimeManager.CreateInstance(snapevent); // запускаем снепшот
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(aicontrol.currentHealth);
    }

    public void HealthSnapshotstart()
    {
        snapInstance.getPlaybackState(out playback);
        
        if (playback != FMOD.Studio.PLAYBACK_STATE.PLAYING) //здоровье меньше 40 мы запускаем снепшот и эту проверку мы включили в Unity во вкладке Events -> Check Health
        {
            
            snapInstance.start(); //запускаем его
            //Debug.Log(playback);
        }
        
    }

    public void HealthSnapshotStop()
    {
        if (control.currentHealth >= 40)
        {
            snapInstance.getPlaybackState(out playback);
            if (playback == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                snapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
            
        }
    }
    
    
}


