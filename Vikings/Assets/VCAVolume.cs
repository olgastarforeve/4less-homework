﻿ using System.Collections;
using System.Collections.Generic;
 using FMOD.Studio;
 using UnityEngine;
 using UnityEngine.UI;

public class VCAVolume : MonoBehaviour
{
    private UnityEngine.UI.Slider ourSlider; // в эту переменную подключаем слайдер FXVolume из меню игры
    private FMOD.Studio.VCA ourVCA;
    [SerializeField] private float ourVolume;
    public string VCAName;
    
    
    // Start is called before the first frame update
    void Start()
    {
        ourVCA = FMODUnity.RuntimeManager.GetVCA("vca:/" + VCAName);
        ourSlider = gameObject.GetComponent<UnityEngine.UI.Slider>();
        
        ourVCA.getVolume(out ourVolume); // обратились к фмоду и спросили какая гвромкость у его ручки в VCA?
        //ourSlider.value = ourVolume;  //считывание настроек из фмода вольюма по дефолту (если не 1 а меньше изначально) например я хоче чтобы не максимум был звук по дефолту а меньше
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void VCAVolumeChange()
    {
        
        ourVCA.setVolume(ourSlider.value); // передает значение из слайдера игры в меню к микшеру в фмоде громкости
        //ourVCA.getVolume(out ourVolume); //передает постоянно обновления из фмода в юнити
        
    }
    
    
    
}
