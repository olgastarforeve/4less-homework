﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class SoundPlayerHit : MonoBehaviour
{

    [FMODUnity.EventRef] public string hitevent;
    public vThirdPersonInput playerInput;

    
    void Start()
    {
        playerInput = GetComponent<vThirdPersonInput>();
        
    }


    public void PlayerHit()
    {
        FMOD.Studio.EventInstance hitInstance = FMODUnity.RuntimeManager.CreateInstance(hitevent);
        //Debug.Log("Hits");
        hitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        
        hitInstance.setParameterByName("Hits", 0);
        hitInstance.start();
        hitInstance.release();
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
